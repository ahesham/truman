#pragma once

#include <math.h>

typedef float vec2[2];
typedef float vec3[3];
typedef float vec4[4];
typedef vec4 mat4[4];

#define X_AXIS (vec3) { 1.0f, 0.0f, 0.0f }
#define Y_AXIS (vec3) { 0.0f, 1.0f, 0.0f }
#define Z_AXIS (vec3) { 0.0f, 0.0f, 1.0f }

#define MAT4_IDENTITY (mat4){ { 1.0f, 0.0f, 0.0f, 0.0f }, \
							  { 0.0f, 1.0f, 0.0f, 0.0f }, \
							  { 0.0f, 0.0f, 1.0f, 0.0f }, \
							  { 0.0f, 0.0f, 0.0f, 1.0f } }
#define MAT4_ZERO (mat4){ { 0.0f, 0.0f, 0.0f, 0.0f }, \
						  { 0.0f, 0.0f, 0.0f, 0.0f }, \
						  { 0.0f, 0.0f, 0.0f, 0.0f }, \
						  { 0.0f, 0.0f, 0.0f, 0.0f } }

static inline void
vec3_scale(vec3 out, vec3 v, float scalar)
{
	out[0] = v[0] * scalar;
	out[1] = v[1] * scalar;
	out[2] = v[2] * scalar;
}

static inline void
vec4_multiply_add_scalar(vec4 out, vec4 a, float scalar)
{
	out[0] += a[0] * scalar;
	out[1] += a[1] * scalar;
	out[2] += a[2] * scalar;
	out[3] += a[3] * scalar;
}

static inline void
mat4_make_identity(mat4 out)
{
	mat4 identity = MAT4_IDENTITY;
	memcpy(out, identity, sizeof(identity));
}

static inline void
mat4_make_zero(mat4 out)
{
	mat4 zero = MAT4_ZERO;
	memcpy(out, zero, sizeof(zero));
}

static inline void
mat4_multiply(mat4 out, mat4 A, mat4 B)
{
	float a00 = A[0][0], a01 = A[0][1], a02 = A[0][2], a03 = A[0][3],
		  a10 = A[1][0], a11 = A[1][1], a12 = A[1][2], a13 = A[1][3],
		  a20 = A[2][0], a21 = A[2][1], a22 = A[2][2], a23 = A[2][3],
		  a30 = A[3][0], a31 = A[3][1], a32 = A[3][2], a33 = A[3][3],

		  b00 = B[0][0], b01 = B[0][1], b02 = B[0][2],
		  b10 = B[1][0], b11 = B[1][1], b12 = B[1][2],
		  b20 = B[2][0], b21 = B[2][1], b22 = B[2][2];

	out[0][0] = a00 * b00 + a10 * b01 + a20 * b02;
	out[0][1] = a01 * b00 + a11 * b01 + a21 * b02;
	out[0][2] = a02 * b00 + a12 * b01 + a22 * b02;
	out[0][3] = a03 * b00 + a13 * b01 + a23 * b02;

	out[1][0] = a00 * b10 + a10 * b11 + a20 * b12;
	out[1][1] = a01 * b10 + a11 * b11 + a21 * b12;
	out[1][2] = a02 * b10 + a12 * b11 + a22 * b12;
	out[1][3] = a03 * b10 + a13 * b11 + a23 * b12;

	out[2][0] = a00 * b20 + a10 * b21 + a20 * b22;
	out[2][1] = a01 * b20 + a11 * b21 + a21 * b22;
	out[2][2] = a02 * b20 + a12 * b21 + a22 * b22;
	out[2][3] = a03 * b20 + a13 * b21 + a23 * b22;

	out[3][0] = a30;
	out[3][1] = a31;
	out[3][2] = a32;
	out[3][3] = a33;
}

static inline void
mat4_make_perspective(mat4 out, float fov, float aspect_ratio, float near, float far)
{
	mat4_make_zero(out);

	const float f = 1.0f / tanf(fov / 2.0f);

	out[0][0] = f / aspect_ratio;
	out[1][1] = f;
	out[2][2] = -((far + near) / (far - near));
	out[2][3] = -1.0f;
	out[3][2] = -((2.0f * far * near) / (far - near));
}

static inline void
mat4_make_rotation(mat4 out, float angle, vec3 axis)
{
	const float cosine = cosf(angle);

	vec3 v, v_sine;
	vec3_scale(v, axis, 1.0f - cosine);
	vec3_scale(v_sine, axis, sinf(angle));

	vec3_scale(out[0], axis, v[0]);
	vec3_scale(out[1], axis, v[1]);
	vec3_scale(out[2], axis, v[2]);

	out[0][0] += cosine;
	out[0][1] += v_sine[2];
	out[0][2] -= v_sine[1];
	out[0][3] = 0.0f;

	out[1][0] -= v_sine[2];
	out[1][1] += cosine;
	out[1][2] += v_sine[0];
	out[1][3] = 0.0f;

	out[2][0] += v_sine[1];
	out[2][1] -= v_sine[0];
	out[2][2] += cosine;
	out[2][3] = 0.0f;

	out[2][3] = 0.0f;
	out[2][3] = 0.0f;
	out[2][3] = 0.0f;
	out[3][3] = 1.0f;
}

static inline void
mat4_translate(mat4 A, vec3 b)
{
	vec4_multiply_add_scalar(A[3], A[0], b[0]);
	vec4_multiply_add_scalar(A[3], A[1], b[1]);
	vec4_multiply_add_scalar(A[3], A[2], b[2]);
}

static inline void
mat4_rotate(mat4 A, float angle, vec3 axis)
{
	mat4 rotation;
	mat4_make_rotation(rotation, angle, axis);
	mat4_multiply(A, A, rotation);
}

static inline void
mat4_invert(mat4 A, const mat4 B)
{
	float s[6];
	float c[6];

	s[0] = B[0][0] * B[1][1] - B[1][0] * B[0][1];
	s[1] = B[0][0] * B[1][2] - B[1][0] * B[0][2];
	s[2] = B[0][0] * B[1][3] - B[1][0] * B[0][3];
	s[3] = B[0][1] * B[1][2] - B[1][1] * B[0][2];
	s[4] = B[0][1] * B[1][3] - B[1][1] * B[0][3];
	s[5] = B[0][2] * B[1][3] - B[1][2] * B[0][3];

	c[0] = B[2][0] * B[3][1] - B[3][0] * B[2][1];
	c[1] = B[2][0] * B[3][2] - B[3][0] * B[2][2];
	c[2] = B[2][0] * B[3][3] - B[3][0] * B[2][3];
	c[3] = B[2][1] * B[3][2] - B[3][1] * B[2][2];
	c[4] = B[2][1] * B[3][3] - B[3][1] * B[2][3];
	c[5] = B[2][2] * B[3][3] - B[3][2] * B[2][3];

	float idet = 1.0f / (s[0] * c[5] - s[1] * c[4] + s[2] * c[3] +
						 s[3] * c[2] - s[4] * c[1] + s[5] * c[0]);

	A[0][0] = ( B[1][1] * c[5] - B[1][2] * c[4] + B[1][3] * c[3]) * idet;
	A[0][1] = (-B[0][1] * c[5] + B[0][2] * c[4] - B[0][3] * c[3]) * idet;
	A[0][2] = ( B[3][1] * s[5] - B[3][2] * s[4] + B[3][3] * s[3]) * idet;
	A[0][3] = (-B[2][1] * s[5] + B[2][2] * s[4] - B[2][3] * s[3]) * idet;

	A[1][0] = (-B[1][0] * c[5] + B[1][2] * c[2] - B[1][3] * c[1]) * idet;
	A[1][1] = ( B[0][0] * c[5] - B[0][2] * c[2] + B[0][3] * c[1]) * idet;
	A[1][2] = (-B[3][0] * s[5] + B[3][2] * s[2] - B[3][3] * s[1]) * idet;
	A[1][3] = ( B[2][0] * s[5] - B[2][2] * s[2] + B[2][3] * s[1]) * idet;

	A[2][0] = ( B[1][0] * c[4] - B[1][1] * c[2] + B[1][3] * c[0]) * idet;
	A[2][1] = (-B[0][0] * c[4] + B[0][1] * c[2] - B[0][3] * c[0]) * idet;
	A[2][2] = ( B[3][0] * s[4] - B[3][1] * s[2] + B[3][3] * s[0]) * idet;
	A[2][3] = (-B[2][0] * s[4] + B[2][1] * s[2] - B[2][3] * s[0]) * idet;

	A[3][0] = (-B[1][0] * c[3] + B[1][1] * c[1] - B[1][2] * c[0]) * idet;
	A[3][1] = ( B[0][0] * c[3] - B[0][1] * c[1] + B[0][2] * c[0]) * idet;
	A[3][2] = (-B[3][0] * s[3] + B[3][1] * s[1] - B[3][2] * s[0]) * idet;
	A[3][3] = ( B[2][0] * s[3] - B[2][1] * s[1] + B[2][2] * s[0]) * idet;
}
