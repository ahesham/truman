#include <stdio.h>
#include <assert.h>
#include <stdbool.h>

#include <fcntl.h>
#include <unistd.h>
#include <sys/stat.h>
#include <sys/types.h>

#include <GLES3/gl3.h>

#include <emscripten/html5.h>

#define CGLTF_IMPLEMENTATION
#include "cgltf.h"

#include "linear_maths.h"

#define OFFSET(x) ((unsigned char*)NULL + (x))

static const char* VertexShaderSource = 
	"#version 300 es\n"
	"\n"
	"layout(location = 0)in vec4 VertexPosition;\n"
	"layout(location = 1)in vec3 VertexNormal;\n"
	"layout(location = 2)in vec2 VertexTexCoord;\n"
	"\n"
	"uniform mat4 ModelMatrix;\n"
	"uniform mat4 ViewMatrix;\n"
	"uniform mat4 ProjectionMatrix;\n"
	"\n"
	"out vec3 normal;\n"
	"\n"
	"void main()\n"
	"{\n"
	"	gl_Position = ProjectionMatrix * ViewMatrix * ModelMatrix * VertexPosition;\n"
	"	normal = (VertexNormal + vec3(1.0)) * 0.5;\n"
	"}\n";

static const char* FragmentShaderSource =
	"#version 300 es\n"
	"precision mediump float;\n"
	"in vec3 normal;\n"
	"in vec2 texcoord;\n"
	"in float x;\n"
	"out vec4 FragColor;\n"
	"void main()\n"
	"{\n"
	"	FragColor = vec4(normal, 1.0);\n"
	"}\n";

typedef struct canvas_type_t
{
	double width;
	double height;
	const char* name;
} canvas_type_t;
static canvas_type_t Canvas;

typedef struct shader_info_t
{
	GLuint program;

	GLuint vao[1];

	struct
	{
		GLint model_matrix;
		GLint view_matrix;
		GLint projection_matrix;
	} locations;
} shader_info_t;
static shader_info_t ShaderInfo;

typedef struct camera_t
{
	mat4 view;
	float fov;
	float near;
	float far;
} camera_t;
static camera_t Camera;

static cgltf_data* Model = NULL;

static inline GLuint
cgltf_index_attribute_type(const cgltf_attribute_type type)
{
	GLuint index = -1;

	switch (type)
	{
		case cgltf_attribute_type_position:
			index = 0;
			break;

		case cgltf_attribute_type_normal:
			index = 1;
			break;

		case cgltf_attribute_type_texcoord:
			index = 2;
			break;

		default:
			assert(0);
	}

	return index;
}

static inline GLenum
cgltf_gl_type(const cgltf_component_type component_type)
{
	GLenum type = -1;

	switch (component_type)
	{
		case cgltf_component_type_r_8:
			type = GL_BYTE;
			break;

		case cgltf_component_type_r_8u:
			type =  GL_UNSIGNED_BYTE;
			break;

		case cgltf_component_type_r_16:
			type = GL_SHORT;
			break;

		case cgltf_component_type_r_16u:
			type = GL_UNSIGNED_SHORT;
			break;

		case cgltf_component_type_r_32u:
			type = GL_UNSIGNED_INT;
			break;

		case cgltf_component_type_r_32f:
			type = GL_FLOAT;
			break;

		default:
			assert(0);
	}

	return type;
}

static inline GLenum
cgltf_gl_mode(const cgltf_primitive_type type)
{
	GLenum mode = -1;

	switch (type)
	{
		case cgltf_primitive_type_points:
			mode = GL_POINTS;
			break;

		case cgltf_primitive_type_lines:
			mode = GL_LINES;
			break;

		case cgltf_primitive_type_line_loop:
			mode = GL_LINE_LOOP;
			break;

		case cgltf_primitive_type_line_strip:
			mode = GL_LINE_STRIP;
			break;

		case cgltf_primitive_type_triangles:
			mode = GL_TRIANGLES;
			break;

		case cgltf_primitive_type_triangle_strip:
			mode = GL_TRIANGLE_STRIP;
			break;

		case cgltf_primitive_type_triangle_fan:
			mode = GL_TRIANGLE_FAN;
			break;

		default:
			assert(0);
	}

	return mode;
}

static bool
create_shader_program(const char** vertex_shader_source, const char** fragment_shader_source)
{
	GLuint vertex_shader = glCreateShader(GL_VERTEX_SHADER);
	glShaderSource(vertex_shader, 1, vertex_shader_source, NULL);
	glCompileShader(vertex_shader);

	GLuint fragment_shader = glCreateShader(GL_FRAGMENT_SHADER);
	glShaderSource(fragment_shader, 1, fragment_shader_source, NULL);
	glCompileShader(fragment_shader);

	ShaderInfo.program = glCreateProgram();
	glAttachShader(ShaderInfo.program, vertex_shader);
	glAttachShader(ShaderInfo.program, fragment_shader);
	glLinkProgram(ShaderInfo.program);

	glDeleteShader(vertex_shader);
	glDeleteShader(fragment_shader);

	glUseProgram(ShaderInfo.program);

	return true;
}

static void
render_background()
{
	glViewport(0, 0, Canvas.width, Canvas.height);
	glClear(GL_COLOR_BUFFER_BIT | GL_DEPTH_BUFFER_BIT);
}

static void
load_model()
{
	int fd = open(GLTF_PATH, O_RDONLY);
	struct stat st;
	fstat(fd, &st);
	off_t model_buffer_size = st.st_size;
	unsigned char* model_buffer = malloc(model_buffer_size);
	read(fd, model_buffer, model_buffer_size);
	close(fd);

	cgltf_result result;
	cgltf_options options = { .type = cgltf_file_type_glb };
	result = cgltf_parse(&options, model_buffer, model_buffer_size, &Model);
	assert(result == cgltf_result_success);
	result = cgltf_load_buffers(&options, Model, NULL);
	assert(result == cgltf_result_success);

	free(model_buffer);
}

static void
setup()
{
	assert(Model->cameras_count == 1);
	assert(Model->meshes_count > 0);
	assert(Model->buffer_views_count > 0);

	// Camera
	{
		cgltf_camera* camera = &Model->cameras[0];
		assert(camera->type == cgltf_camera_type_perspective);

		ShaderInfo.locations.view_matrix = glGetUniformLocation(ShaderInfo.program, "ViewMatrix");
		cgltf_node* camera_node = &Model->nodes[1];
		cgltf_node_transform_world(camera_node, (float*)Camera.view);
		mat4 view_matrix;
		mat4_invert(view_matrix, Camera.view);
		glUniformMatrix4fv(ShaderInfo.locations.view_matrix, 1,
					   GL_FALSE, (const GLfloat*)view_matrix);

		Camera.fov = camera->data.perspective.yfov;
		Camera.near = camera->data.perspective.znear;
		Camera.far = camera->data.perspective.zfar;
	}

	glGenVertexArrays(3, ShaderInfo.vao);

	// Meshes
	for (cgltf_size i = 0; i < Model->meshes_count; ++i)
	{
		glBindVertexArray(ShaderInfo.vao[i]);

		cgltf_mesh* mesh = &Model->meshes[i];

		// Fill mesh data
		assert(mesh->primitives_count > 0);
		for (cgltf_size j = 0; j < mesh->primitives_count; ++j)
		{
			cgltf_primitive* primitive = &mesh->primitives[j];

			assert(primitive->attributes_count > 0);
			for (cgltf_size k = 0; k < primitive->attributes_count; ++k)
			{
				cgltf_attribute* attribute = &primitive->attributes[k];
				cgltf_accessor* accessor = attribute->data;

				GLuint vbo;
				glGenBuffers(1, &vbo);
				glBindBuffer(GL_ARRAY_BUFFER, vbo);
				glBufferData(GL_ARRAY_BUFFER,
							 accessor->buffer_view->size,
							 (unsigned char*)accessor->buffer_view->buffer->data + accessor->buffer_view->offset,
							 GL_STATIC_DRAW);

				glEnableVertexAttribArray(cgltf_index_attribute_type(attribute->type));
				glVertexAttribPointer(cgltf_index_attribute_type(attribute->type), cgltf_num_components(accessor->type),
									  cgltf_gl_type(accessor->component_type), accessor->normalized,
									  accessor->stride, OFFSET(accessor->offset));
			}

			cgltf_accessor* accessor = primitive->indices;

			GLuint vbo;
			glGenBuffers(1, &vbo);
			glBindBuffer(GL_ELEMENT_ARRAY_BUFFER, vbo);
			glBufferData(GL_ELEMENT_ARRAY_BUFFER,
						 accessor->buffer_view->size,
						 (unsigned char*)accessor->buffer_view->buffer->data + accessor->buffer_view->offset,
						 GL_STATIC_DRAW);
		}

		glBindVertexArray(0);
	}

	// Uniforms
	ShaderInfo.locations.model_matrix = glGetUniformLocation(ShaderInfo.program, "ModelMatrix");
	ShaderInfo.locations.projection_matrix = glGetUniformLocation(ShaderInfo.program, "ProjectionMatrix");
}

static void
render()
{
	// Clear the view and depth buffer
	glViewport(0, 0, Canvas.width, Canvas.height);
	glClear(GL_COLOR_BUFFER_BIT | GL_DEPTH_BUFFER_BIT);

	// Update the perspective matrix
	const float aspect_ratio = Canvas.width / Canvas.height;
	mat4 perspective;
	mat4_make_perspective(perspective, Camera.fov, aspect_ratio, Camera.near, Camera.far);
	glUniformMatrix4fv(ShaderInfo.locations.projection_matrix, 1,
					   GL_FALSE, (const GLfloat*)perspective);

	// Draw the model
	{
		for (cgltf_size i = 0; i < Model->nodes_count; ++i)
		{
			cgltf_node* node = &Model->nodes[i];
			cgltf_mesh* mesh = node->mesh;
			if (mesh == NULL) continue;

			const int index = i > 0 ? i -1 : i;
			glBindVertexArray(ShaderInfo.vao[index]);

			mat4 node_matrix;
			mat4_make_identity(node_matrix);
			cgltf_node_transform_world(node, (float*)node_matrix);
			glUniformMatrix4fv(ShaderInfo.locations.model_matrix, 1,
							   GL_FALSE, (const GLfloat*)node_matrix);

			assert(mesh->primitives_count > 0);
			for (cgltf_size j = 0; j < mesh->primitives_count; ++j)
			{
				cgltf_primitive* primitive = mesh->primitives + j;
				cgltf_accessor* accessor = primitive->indices;

				glDrawElements(cgltf_gl_mode(primitive->type), accessor->count,
							   cgltf_gl_type(accessor->component_type),
							   OFFSET(accessor->offset));
			}

			glBindVertexArray(0);
		}
	}
}

static EM_BOOL
resize_callback(int event_type, const EmscriptenUiEvent* ui_event, void* user_data)
{
	(void) event_type;
	(void) ui_event;
	(void) user_data;

	emscripten_get_element_css_size(Canvas.name, &Canvas.width, &Canvas.height);
	emscripten_set_canvas_element_size(Canvas.name, Canvas.width, Canvas.height);

	return EM_TRUE;
}

static bool
init(const char* canvas_name)
{
	emscripten_get_element_css_size(canvas_name, &Canvas.width, &Canvas.height);
	Canvas.name = canvas_name;

	emscripten_set_canvas_element_size(Canvas.name, Canvas.width, Canvas.height);
	emscripten_set_resize_callback(EMSCRIPTEN_EVENT_TARGET_WINDOW, NULL, EM_FALSE, resize_callback);

	EmscriptenWebGLContextAttributes attributes;
	emscripten_webgl_init_context_attributes(&attributes);
	attributes.depth = EM_TRUE;
	attributes.antialias = EM_TRUE;
	attributes.powerPreference = EM_WEBGL_POWER_PREFERENCE_HIGH_PERFORMANCE;
	attributes.failIfMajorPerformanceCaveat = EM_TRUE;
	attributes.majorVersion = 2;

	// Try to create a WebGL 2.0 context, if that fails, fallback to WebGL 1.0
	EMSCRIPTEN_WEBGL_CONTEXT_HANDLE handle = emscripten_webgl_create_context(Canvas.name, &attributes);
	if (handle == 0)
	{
		fprintf(stderr, "This browser does not support WebGL 2.0.\n");
		attributes.majorVersion = 1;
		handle = emscripten_webgl_create_context(Canvas.name, &attributes);
		if (handle == 0)
		{
			fprintf(stderr, "Failed to create a WebGL 1.0 context.\n");
			return false;
		}
	}

	emscripten_webgl_make_context_current(handle);

	// GL options
	{
		// Culling
		glEnable(GL_CULL_FACE);
		glCullFace(GL_BACK);

		// Depth
		glEnable(GL_DEPTH_TEST);
		glDepthFunc(GL_LEQUAL);
		glClearDepthf(1.0f);

		// Clear
		glClearColor(0.604f, 0.796f, 1.0f, 1.0f);
	}

	return true;
}

int
main(int argc, char* argv[])
{
	init("#canvas");

	render_background();

	create_shader_program(&VertexShaderSource, &FragmentShaderSource);

	load_model();

	setup();

	emscripten_set_main_loop(render, 0, 1);

	return 0;
}
