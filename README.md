# "The Truman Show" Digital Diorama
A tribute to one of my favourite films.

The diorama can be viewed [here](https://3a2l.com/diorama/truman/).

## About "The Truman Show"
### Synopsis
An insurance salesman is oblivious of the fact that his entire life is a TV show and his family members are mere actors. As he starts noticing things and uncovers the truth, he decides to escape.

### Trailer
[![Trailer Thumbnail](https://img.youtube.com/vi/dlnmQbPGuls/0.jpg)](https://www.youtube.com/watch?v=dlnmQbPGuls)
